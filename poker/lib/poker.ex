defmodule Poker do
    @high 1
    @pair 2
    @two_pair 3
    @three_of_a_kind 4
    @straight 5
    @flush 6
    @full_house 7
    @four_of_a_kind 8
    @straight_flush 9

    @face_to_value %{"1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5,
    "6" => 6, "7" => 7, "8" => 8, "9" => 9, "10" => 10, "J" => 11, "Q" => 12,
    "K" => 13, "A" => 14}

    @spec best_hand(list(list(String.t()))) :: list(list(String.t()))
    def best_hand(input) do
        input
        |> Enum.map(fn hand ->
            hand
            |> parse_hand()
            |> rank_hand(hand)
        end)
        |> Enum.group_by(fn {rank, _hand} -> rank end, fn {_rank, hand} -> hand end)
        |> Enum.sort()
        |> Enum.reverse()
        |> Enum.take(1)
        |> Enum.flat_map(fn {_rank, hand} -> hand end)
    end

    def parse_hand(cards) do
        Enum.map(cards, fn card ->
            {face, suit} = String.split_at(card, -1)
            {@face_to_value[face], String.to_atom(suit)}
        end)
    end

    def rank_hand(cards, orig) do
        rank = cards
        |> just_the_values()
        |> Enum.frequencies()
        |> Enum.map(fn {value, freq} -> {freq, value} end)
        |> Enum.sort()
        |> Enum.reverse()
        |> get_ranking(cards)

        {rank, orig}
    end

    def get_ranking([{2, pair}, {1, k1}, {1, k2}, {1, k3}], _cards), do: [@pair, pair, k1, k2, k3]
    def get_ranking([{2, high}, {2, low}, {1, k1}], _cards), do: [@two_pair, high, low, k1]
    def get_ranking([{3, three}, {1, k1}, {1, k2}], _cards), do: [@three_of_a_kind, three, k1, k2]
    def get_ranking([{3, three}, {2, two}], _cards), do: [@full_house, three, two]
    def get_ranking([{4, four}, {1, k1}], _cards), do: [@four_of_a_kind, four, k1]
    def get_ranking(groups = [{1, high}, {1, k1}, {1, k2}, {1, k3}, {1, k4}], cards) do
        case {flush?(cards), straight?(groups), high} do
            {false, false, _high} -> [@high, high, k1, k2, k3, k4]
            {true, false, _high} -> [@flush, high, k1, k2, k3, k4]
            {true, true, 14} -> [@straight_flush, k1, k2, k3, k4, 1] # ace low
            {true, true, _high} -> [@straight_flush, high, k1, k2, k3, k4]
            {false, true, 14} -> [@straight, k1, k2, k3, k4, 1] # ace low
            {false, true, _high} -> [@straight, high, k1, k2, k3, k4]
        end
    end

    def flush?(cards) do
        1 == cards
        |> just_the_suits()
        |> Enum.dedup()
        |> Enum.count()
    end

    def straight?([{_, 14}, {_, 5}, {_, 4}, {_, 3}, {_, 2}]), do: true
    def straight?([{_, high}, _, _, _, {_, low}]), do: high - low == 4
    def straight?(_), do: false

    def just_the_values(cards), do: Enum.map(cards, fn {value, _suit} -> value end)

    def just_the_suits(cards), do: Enum.map(cards, fn {_value, suit} -> suit end)
end
